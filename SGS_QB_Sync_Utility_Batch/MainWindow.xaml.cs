﻿using SGS_QB_Sync_Utility_Batch.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SGS_QB_Sync_Utility_Batch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonSyncWithQuickbooks_Click(object sender, RoutedEventArgs e)
        {
            if (!GlobalClass.connectionOpen)
            {
                if (!GlobalClass.OpenQuickBooksDesktopConnection())
                {
                    MessageBox.Show("Couldn't establish QuickBooks connection, please check if QuickBooks desktop is running on same system.", "SGS QuickBooks Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            ButtonSyncWithQuickbooks.IsEnabled = false;
            //Starting the thread to sync data with QuickBooks
            Thread syncdatathread = new Thread(SyncDataWithQuickBooks);

            syncdatathread.Start();
        }


        private void SyncDataWithQuickBooks()
        {

        }

            private void CloseWindow()
        {
            GlobalClass.CloseQuickBooksConnection();

            //Resetting the global class variables
            GlobalClass.clubid = 0;
            GlobalClass.DefaultQBAssetAccount = "";
            GlobalClass.DefaultQBCOGSAccount = "";
            GlobalClass.DefaultQBIncomeAccount = "";

            this.Hide();
            LoginWindow login = new LoginWindow();
            login.Show();
        }


        private void ButtonLogout_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CloseWindow();
        }
    }
}
