﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGS_QB_Sync_Utility_Batch.Models
{
    public class QuickBooksAccount
    {
        public int Id { get; set; }
        public int ClubId { get; set; }
        public string CompanyId { get; set; }
        public string ConnectionKey { get; set; }
        public string ConnectionSecret { get; set; }
        public string DefaultQBIncomeAccount { get; set; }
        public string DefaultQBAssetAccount { get; set; }
        public string DefaultQBCOGSAccount { get; set; }
        public string DefaultQBGreenFeeIncomeAccount { get; set; }
        public string CreditVoucherSalesIncomeAccount { get; set; }
        public string TaxAgencyVendor { get; set; }
        public string SalesTaxAccount { get; set; }
        public string SalesTaxId { get; set; }
        public bool QBBarcodesEnabled { get; set; }
        public string TeeBookingQBId { get; set; }
        public string TeeGroupBookingQBId { get; set; }
        public string MemberShipItemQBId { get; set; }
        public string DiscountItemQBId { get; set; }
        public string MembershipPaymentQBId { get; set; }
        public string BraintreePaymentQBId { get; set; }
        public string BluePayPaymentQBId { get; set; }
        public string CashPaymentQBId { get; set; }
        public string AuthorizeNetPaymentQBId { get; set; }
        public string CreditVoucherPaymentQBId { get; set; }
        public string CheckPaymentQBId { get; set; }
        public string BasysPaymentQBId { get; set; }
        public string FirstPayPaymentQBId { get; set; }
        public string AnonymousCustomerQBId { get; set; }
        public string TipQBId { get; set; }
        public string GiftVoucherItemQBId { get; set; }
        public string RefundVoucherItemQBId { get; set; }
    }

    public class QuickBooksAccountRecord
    {
        public QuickBooksAccount QuickBooksAccount { get; set; }
    }

    public class QuickBooksAccountRootObject
    {
        public QuickBooksAccountRecord Record { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
