﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGS_QB_Sync_Utility_Batch.Models
{
    public class Module
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool AccessFlag { get; set; }
    }

    public class ModuleGroup
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public List<Module> Modules { get; set; }
    }

    public class Role
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Status { get; set; }
        public int CourseUserId { get; set; }
        public List<ModuleGroup> ModuleGroups { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
        public string UserName { get; set; }
        public string Image { get; set; }
        public Role Role { get; set; }
    }

    public class Cours
    {
        public int ID { get; set; }
        public string COURSE_NAME { get; set; }
    }

    public class TerminalLocation
    {
        public int Id { get; set; }
        public int ClubId { get; set; }
        public string Title { get; set; }
        public List<object> Terminals { get; set; }
    }

    public class Club
    {
        public int ClubId { get; set; }
        public string ClubName { get; set; }
        public string ClubLogo { get; set; }
        public int timeInterval { get; set; }
        public List<Cours> Courses { get; set; }
        public List<TerminalLocation> TerminalLocations { get; set; }
    }

    public class Cours2
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Club2
    {
        public int ClubId { get; set; }
        public string Name { get; set; }
        public List<Cours2> Courses { get; set; }
    }

    public class ManagementCompany
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<Club2> Clubs { get; set; }
    }

    public class UserRecord
    {
        public User User { get; set; }
        public Club Club { get; set; }
        public List<ManagementCompany> ManagementCompany { get; set; }
    }

    public class UserRootObject
    {
        public UserRecord Record { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
