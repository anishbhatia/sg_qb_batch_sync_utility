﻿using log4net;
using Newtonsoft.Json;
using SGS_QB_Sync_Utility_Batch.App_Code;
using SGS_QB_Sync_Utility_Batch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SGS_QB_Sync_Utility_Batch
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        //Declare an instance for log4net
        private static readonly ILog logger = LogManager.GetLogger(typeof(LoginWindow));

        public LoginWindow()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            logger.Info("Login window loaded.");

            InsertJournalEntryQuickBooks j = new InsertJournalEntryQuickBooks();
            j.DoJournalEntryAdd();
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                string content = GlobalClass.Login(TextBoxUsername.Text, TextBoxPassword.Password);

                //Converting Json string received from API to C# object
                var rootObject = JsonConvert.DeserializeObject<UserRootObject>(content);

                if (rootObject != null)
                {
                    if (rootObject.Status == 1)
                    {
                        if (rootObject.Record != null)
                        {
                            if (rootObject.Record.Club != null)
                            {

                                GlobalClass.clubid = rootObject.Record.Club.ClubId;
                                GlobalClass.GolfCourseName = rootObject.Record.Club.ClubName;
                                logger.Info("Login successful" + " buttonlogin_Click clubid: " + GlobalClass.clubid);
                                this.Hide();

                                MainWindow window = new MainWindow();
                                window.Show();

                                TextBoxPassword.Password = "";
                                TextBoxUsername.Text = "";
                            }
                            else
                                MessageBox.Show("Login username/password incorrect.", "SGS Login", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                            MessageBox.Show("Login username/password incorrect.", "SGS Login", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        logger.Info("Login not successful" + " buttonlogin_Click username: " + TextBoxUsername.Text);
                        MessageBox.Show("Login username/password incorrect.", "SGS Login", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                    MessageBox.Show("Login username/password incorrect.", "SGS Login", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Login failed.", "SGS Login", MessageBoxButton.OK, MessageBoxImage.Error);
                logger.Error("Login failed : " + ex.Message + " buttonlogin_Click username: " + TextBoxUsername.Text);
            }
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            GlobalClass.CloseQuickBooksConnection();
            this.Close();
            Application.Current.Shutdown();
        }

    }
}
