﻿using Microsoft.HockeyApp;
using SGS_QB_Sync_Utility_Batch.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SGS_QB_Sync_Utility_Batch
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected async override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            try
            {
                HockeyClient.Current.Configure(ConfigurationManager.AppSettings["HockeyAppIdentifier"])
                 .SetExceptionDescriptionLoader((Exception ex) =>
                 {
                     string descriptionString = "";
                     try
                     {
                         if (GlobalClass.clubid != 0)
                         {
                             descriptionString += "ClubId: " + GlobalClass.clubid;
                         }
                     }
                     catch (Exception exception)
                     {

                     }
                     return descriptionString;
                 }); ;

                await HockeyClient.Current.SendCrashesAsync(true);

                //Connection with Quickbooks desktop
                bool connectionsuccess = GlobalClass.OpenQuickBooksDesktopConnection();
                if (!connectionsuccess)
                    MessageBox.Show("Failed to establish connection with Quickbooks.", "SGS QuickBooks Connection", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            catch (Exception ex)
            {
                //File.AppendAllText(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Log.txt", Environment.NewLine + DateTime.Now + " " + ex.Message + " " + ex.InnerException + " App.xaml.cs.OnStartup");
                MessageBox.Show(ex.Message, "SGS QuickBooks Connection", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
