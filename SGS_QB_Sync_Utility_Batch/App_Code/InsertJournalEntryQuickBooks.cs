﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SGS_QB_Sync_Utility_Batch.App_Code
{
    class InsertJournalEntryQuickBooks
    {
        //Declare an instance for log4net
        private static readonly ILog logger = LogManager.GetLogger(typeof(InsertJournalEntryQuickBooks));

        public InsertJournalEntryQuickBooks()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public void DoJournalEntryAdd()
        {
            try
            {
                XmlDocument requestXmlDoc = GlobalClass.GetXMLDocument();

                BuildJournalEntryAddRq(requestXmlDoc, GlobalClass.GetInnerXMLforQB(requestXmlDoc));

                //Send the request and get the response from QuickBooks
                string responseStr = GlobalClass.SendRequestToQuickBooksDesktop(requestXmlDoc);

                WalkJournalEntryAddRs(responseStr);

            }
            catch (Exception e)
            {
                logger.Error(e.Message + " " + e.StackTrace);
            }
        }


        void BuildJournalEntryAddRq(XmlDocument doc, XmlElement parent)
        {

            //Create JournalEntryAddRq aggregate and fill in field values for it
            XmlElement JournalEntryAddRq = doc.CreateElement("JournalEntryAddRq");
            parent.AppendChild(JournalEntryAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            XmlElement JournalEntryAdd = doc.CreateElement("JournalEntryAdd");
            JournalEntryAddRq.AppendChild(JournalEntryAdd);
            //Set field value for TxnDate <!-- optional -->
            JournalEntryAdd.AppendChild(GlobalClass.MakeSimpleElem(doc, "TxnDate", Convert.ToString(Convert.ToDateTime("2018-05-15").ToString("yyyy-MM-dd"))));

            //Set field value for RefNumber <!-- optional -->
            //JournalEntryAdd.AppendChild(GlobalClass.MakeSimpleElem(doc, "RefNumber", "ab"));
            
            //Begin OR
            string ORElementType = "JournalDebitLine";
            
            if (ORElementType == "JournalDebitLine")
            {

                //Create JournalDebitLine aggregate and fill in field values for it
                XmlElement JournalDebitLine = doc.CreateElement("JournalDebitLine");
                JournalEntryAdd.AppendChild(JournalDebitLine);
                //Set field value for TxnLineID <!-- optional -->
                //JournalDebitLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "TxnLineID", "200000-1011023419"));

                //Create AccountRef aggregate and fill in field values for it
                XmlElement AccountRef = doc.CreateElement("AccountRef");
                JournalDebitLine.AppendChild(AccountRef);
                //Set field value for ListID <!-- optional -->
                //AccountRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "ListID", "200000-1011023419"));
                //Set field value for FullName <!-- optional -->
                AccountRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "FullName", "Construction Income"));
                //Done creating AccountRef aggregate

                //Set field value for Amount <!-- optional -->
                JournalDebitLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "Amount", Convert.ToDouble(10.5).ToString("F")));
                //Set field value for Memo <!-- optional -->
                JournalDebitLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "Memo", "Testing of jounral debit"));

                

                //Set field value for BillableStatus <!-- optional -->
                //JournalDebitLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "BillableStatus", "Billable"));
                //Done creating JournalDebitLine aggregate

            }
            ORElementType = "JournalCreditLine";
            if (ORElementType == "JournalCreditLine")
            {

                //Create JournalCreditLine aggregate and fill in field values for it
                XmlElement JournalCreditLine = doc.CreateElement("JournalCreditLine");
                JournalEntryAdd.AppendChild(JournalCreditLine);
                //Set field value for TxnLineID <!-- optional -->
                //JournalCreditLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "TxnLineID", "200000-1011023419"));

                //Create AccountRef aggregate and fill in field values for it
                XmlElement AccountRef = doc.CreateElement("AccountRef");
                JournalCreditLine.AppendChild(AccountRef);
                //Set field value for ListID <!-- optional -->
                //AccountRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "ListID", "200000-1011023419"));
                //Set field value for FullName <!-- optional -->
                AccountRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "FullName", "Automobile"));
                //Done creating AccountRef aggregate

                //Set field value for Amount <!-- optional -->
                JournalCreditLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "Amount", Convert.ToDouble(10.5).ToString("F")));
                //Set field value for Memo <!-- optional -->
                JournalCreditLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "Memo", "Testing of jounral credit"));

                //Set field value for BillableStatus <!-- optional -->
                //JournalCreditLine.AppendChild(GlobalClass.MakeSimpleElem(doc, "BillableStatus", "Billable"));
                //Done creating JournalCreditLine aggregate

            }
        }




        void WalkJournalEntryAddRs(string response)
        {
            //Parse the response XML string into an XmlDocument
            XmlDocument responseXmlDoc = new XmlDocument();
            responseXmlDoc.LoadXml(response);

            //Get the response for our request
            XmlNodeList JournalEntryAddRsList = responseXmlDoc.GetElementsByTagName("JournalEntryAddRs");
            if (JournalEntryAddRsList.Count == 1)
            {
                XmlNode responseNode = JournalEntryAddRsList.Item(0);
                //Check the status code, info, and severity
                XmlAttributeCollection rsAttributes = responseNode.Attributes;
                string statusCode = rsAttributes.GetNamedItem("statusCode").Value;
                string statusSeverity = rsAttributes.GetNamedItem("statusSeverity").Value;
                string statusMessage = rsAttributes.GetNamedItem("statusMessage").Value;

                //status code = 0 all OK, > 0 is warning
                if (Convert.ToInt32(statusCode) == 0)
                {
                    XmlNodeList JournalEntryRetList = responseNode.SelectNodes("//JournalEntryRet");//XPath Query
                    for (int i = 0; i < JournalEntryRetList.Count; i++)
                    {
                        XmlNode JournalEntryRet = JournalEntryRetList.Item(i);
                        //WalkJournalEntryRet(JournalEntryRet);
                    }
                }
            }
        }

    }
}
