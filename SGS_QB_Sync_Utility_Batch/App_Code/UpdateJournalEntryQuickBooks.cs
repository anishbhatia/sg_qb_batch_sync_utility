﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SGS_QB_Sync_Utility_Batch.App_Code
{
    class UpdateJournalEntryQuickBooks
    {
        //Declare an instance for log4net
        private static readonly ILog logger = LogManager.GetLogger(typeof(UpdateJournalEntryQuickBooks));

        public UpdateJournalEntryQuickBooks()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public void DoJournalEntryMod()
        {
            try
            {
                XmlDocument requestXmlDoc = GlobalClass.GetXMLDocument();

                BuildJournalEntryModRq(requestXmlDoc, GlobalClass.GetInnerXMLforQB(requestXmlDoc));

                //Send the request and get the response from QuickBooks
                string responseStr = GlobalClass.SendRequestToQuickBooksDesktop(requestXmlDoc);

                WalkJournalEntryModRs(responseStr);

            }
            catch (Exception e)
            {
                logger.Error(e.Message + " " + e.StackTrace);
            }
        }


        void BuildJournalEntryModRq(XmlDocument doc, XmlElement parent)
        {

            //Create JournalEntryModRq aggregate and fill in field values for it
            XmlElement JournalEntryModRq = doc.CreateElement("JournalEntryModRq");
            parent.AppendChild(JournalEntryModRq);

            //Create JournalEntryMod aggregate and fill in field values for it
            XmlElement JournalEntryMod = doc.CreateElement("JournalEntryMod");
            JournalEntryModRq.AppendChild(JournalEntryMod);
            //Set field value for TxnID <!-- required -->
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "TxnID", "200000-1011023419"));
            //Set field value for EditSequence <!-- required -->
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "EditSequence", "ab"));
            //Set field value for TxnDate <!-- optional -->
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "TxnDate", "2007-12-15"));
            //Set field value for RefNumber <!-- optional -->
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "RefNumber", "ab"));
            //Set field value for IsAdjustment <!-- optional -->
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "IsAdjustment", "1"));
            //Set field value for IsAmountsEnteredInHomeCurrency <!-- optional -->
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "IsAmountsEnteredInHomeCurrency", "1"));

            //Create CurrencyRef aggregate and fill in field values for it
            XmlElement CurrencyRef = doc.CreateElement("CurrencyRef");
            JournalEntryMod.AppendChild(CurrencyRef);
            //Set field value for ListID <!-- optional -->
            CurrencyRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "ListID", "200000-1011023419"));
            //Set field value for FullName <!-- optional -->
            CurrencyRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "FullName", "ab"));
            //Done creating CurrencyRef aggregate

            //Set field value for ExchangeRate <!-- optional -->
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "ExchangeRate", "FLOATTYPE"));

            //Create JournalLineMod aggregate and fill in field values for it
            // May create more than one of these aggregates if needed
            XmlElement JournalLineMod = doc.CreateElement("JournalLineMod");
            JournalEntryMod.AppendChild(JournalLineMod);
            //Set field value for TxnLineID <!-- required -->
            JournalLineMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "TxnLineID", "200000-1011023419"));
            //Set field value for JournalLineType <!-- optional -->
            JournalLineMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "JournalLineType", "Debit"));

            //Create AccountRef aggregate and fill in field values for it
            XmlElement AccountRef = doc.CreateElement("AccountRef");
            JournalLineMod.AppendChild(AccountRef);
            //Set field value for ListID <!-- optional -->
            AccountRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "ListID", "200000-1011023419"));
            //Set field value for FullName <!-- optional -->
            AccountRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "FullName", "ab"));
            //Done creating AccountRef aggregate

            //Set field value for Amount <!-- optional -->
            JournalLineMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "Amount", "10.01"));
            //Set field value for Memo <!-- optional -->
            JournalLineMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "Memo", "ab"));

            //Create EntityRef aggregate and fill in field values for it
            XmlElement EntityRef = doc.CreateElement("EntityRef");
            JournalLineMod.AppendChild(EntityRef);
            //Set field value for ListID <!-- optional -->
            EntityRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "ListID", "200000-1011023419"));
            //Set field value for FullName <!-- optional -->
            EntityRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "FullName", "ab"));
            //Done creating EntityRef aggregate


            //Create ClassRef aggregate and fill in field values for it
            XmlElement ClassRef = doc.CreateElement("ClassRef");
            JournalLineMod.AppendChild(ClassRef);
            //Set field value for ListID <!-- optional -->
            ClassRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "ListID", "200000-1011023419"));
            //Set field value for FullName <!-- optional -->
            ClassRef.AppendChild(GlobalClass.MakeSimpleElem(doc, "FullName", "ab"));
            //Done creating ClassRef aggregate

            //Set field value for BillableStatus <!-- optional -->
            JournalLineMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "BillableStatus", "Billable"));
            //Done creating JournalLineMod aggregate

            //Set field value for undefined
            JournalEntryMod.AppendChild(GlobalClass.MakeSimpleElem(doc, "undefined", "undefined"));
            //Done creating JournalEntryMod aggregate

            //Set field value for IncludeRetElement <!-- optional, may repeat -->
            JournalEntryModRq.AppendChild(GlobalClass.MakeSimpleElem(doc, "IncludeRetElement", "ab"));
            //Done creating JournalEntryModRq aggregate

        }




        void WalkJournalEntryModRs(string response)
        {
            //Parse the response XML string into an XmlDocument
            XmlDocument responseXmlDoc = new XmlDocument();
            responseXmlDoc.LoadXml(response);

            //Get the response for our request
            XmlNodeList JournalEntryModRsList = responseXmlDoc.GetElementsByTagName("JournalEntryModRs");
            if (JournalEntryModRsList.Count == 1) 
            {
                XmlNode responseNode = JournalEntryModRsList.Item(0);
                //Check the status code, info, and severity
                XmlAttributeCollection rsAttributes = responseNode.Attributes;
                string statusCode = rsAttributes.GetNamedItem("statusCode").Value;
                string statusSeverity = rsAttributes.GetNamedItem("statusSeverity").Value;
                string statusMessage = rsAttributes.GetNamedItem("statusMessage").Value;

                //status code = 0 all OK, > 0 is warning
                if (Convert.ToInt32(statusCode) == 0)
                {
                    XmlNodeList JournalEntryRetList = responseNode.SelectNodes("//JournalEntryRet");//XPath Query
                    for (int i = 0; i < JournalEntryRetList.Count; i++)
                    {
                        XmlNode JournalEntryRet = JournalEntryRetList.Item(i);
                        //WalkJournalEntryRet(JournalEntryRet);
                    }
                }
            }
        }


    }
}
