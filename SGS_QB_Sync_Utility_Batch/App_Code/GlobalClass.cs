﻿using Interop.QBXMLRP2Lib;
using log4net;
using Newtonsoft.Json;
using SGS_QB_Sync_Utility_Batch.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace SGS_QB_Sync_Utility_Batch.App_Code
{
    class GlobalClass
    {
        

        public static int clubid = 0;
        public static string baseurl = Convert.ToString(ConfigurationManager.AppSettings["APIBaseURL"]);
        public static string apikey = Convert.ToString(ConfigurationManager.AppSettings["APIKey"]);

        public static string GolfCourseName = string.Empty;
        public static string DefaultQBIncomeAccount = string.Empty;
        public static string DefaultQBAssetAccount = string.Empty;
        public static string DefaultQBCOGSAccount = string.Empty;
        public static string DefaultSalesTaxId = string.Empty;
        public static string DefaultSalesTaxAccount = string.Empty;
        public static string TeeBookingQBId = string.Empty;
        public static string CreditVoucherSalesIncomeAccount = string.Empty;
        public static string TeeGroupBookingQBId = string.Empty;
        public static string AnonymousCustomerQBId = string.Empty;
        public static string TipQBId = string.Empty;
        public static string MemberShipItemQBId = string.Empty;
        public static string DiscountItemQBId = string.Empty;
        public static string GiftVoucherItemQBId = string.Empty;
        public static string RefundVoucherItemQBId = string.Empty;

        public static string MembershipPaymentQBId = string.Empty;
        public static string BraintreePaymentQBId = string.Empty;
        public static string BluePayPaymentQBId = string.Empty;
        public static string CashPaymentQBId = string.Empty;
        public static string AuthorizeNetPaymentQBId = string.Empty;
        public static string CreditVoucherPaymentQBId = string.Empty;
        public static string CheckPaymentQBId = string.Empty;
        public static string BasysPaymentQBId = string.Empty;
        public static string FirstPayPaymentQBId = string.Empty;

        public static bool IsBarCodeEnabled = false;

        //Quickbooks desktop connection variables
        public static bool sessionBegun = false;
        public static bool connectionOpen = false;
        public static RequestProcessor2 rp = null;
        public static string ticket = null;

        //Endpoints
        public const string login = "CourseUsers/QBLogin";
        public const string InventorySave = "InventoryItemQBDetails/Save";
        public const string InventoryGet = "InventoryItemQBDetails";
        public const string CustomerSave = "CustomerQBDetails/Save";
        public const string CustomerGet = "CustomerQBDetails";
        public const string OrderSave = "OrderQBDetails/Save";
        public const string OrderGet = "OrderQBDetails";
        public const string CreditVoucherSave = "CreditVoucherQBDetails/Save";
        public const string CreditVoucherGet = "CreditVoucherQBDetails";
        public const string PaymentMethodSave = "PaymentMethodQBDetails/Save";
        public const string PaymentTypes = "PaymentMethodQBDetails/GetPaymentTypesByClubId";
        public const string Quickbooksaccountedit = "QuickBooksAccount/Edit";
        public const string Quickbooksaccountget = "QuickBooksAccount/GetByClub";

        /// <summary>
        /// This function closes connection with QuickBooks and ends the current QuickBooks session
        /// </summary>
        public static void CloseQuickBooksConnection()
        {
            if (connectionOpen)
            {
                if (sessionBegun)
                {
                    if (rp != null)
                    {
                        if (ticket != null)
                        {
                            //End the session and close the connection to QuickBooks
                            rp.EndSession(ticket);
                            sessionBegun = false;
                            rp.CloseConnection();
                            connectionOpen = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This function opens connection with QuickBooks and starts a new QuickBooks session
        /// </summary>
        /// <returns></returns>
        public static bool OpenQuickBooksDesktopConnection()
        {
            try
            {
                if (!connectionOpen)
                {
                    if (!sessionBegun)
                    {
                        //Create the Request Processor object
                        rp = new RequestProcessor2();

                        //Connect to QuickBooks and begin a session
                        rp.OpenConnection2("", GlobalClass.GetApplicationName(), QBXMLRPConnectionType.localQBD);
                        connectionOpen = true;
                        ticket = rp.BeginSession("", QBFileMode.qbFileOpenDoNotCare);
                        sessionBegun = true;
                    }
                }
            }
            catch (Exception ex)
            {
                connectionOpen = false;
            }
            return connectionOpen;
        }

        public static string SendRequestToQuickBooksDesktop(XmlDocument requestXmlDoc)
        {
            //Send the request and get the response from QuickBooks
            string responseStr = rp.ProcessRequest(ticket, requestXmlDoc.OuterXml);

            return responseStr;
        }

        public static XmlDocument GetXMLDocument()
        {
            //Create the XML document to hold our request
            XmlDocument requestXmlDoc = new XmlDocument();
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"13.0\""));

            return requestXmlDoc;
        }

        public static XmlElement GetInnerXMLforQB(XmlDocument requestXmlDoc)
        {
            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            return inner;
        }

        /// <summary>
        /// Application Name to be sent to Quickbooks which will be displayed to end user
        /// </summary>
        /// <returns></returns>
        public static string GetApplicationName()
        {
            return "SGS Sync for QuickBooks";
        }

        public static string Login(string username, string password)
        {
            //Calling POS API to authenticate user credentials
            Encoding encoding = new UTF8Encoding();
            string postData = "{\"UserName\":\"" + username + "\",\"Password\":\"" + password + "\"}";
            byte[] data = encoding.GetBytes(postData);

            string endpoint = baseurl + login;

            return CallPostAPI(endpoint, data);
        }

        public static XmlElement MakeSimpleElem(XmlDocument doc, string tagName, string tagVal)
        {
            XmlElement elem = doc.CreateElement(tagName);
            elem.InnerText = tagVal;
            return elem;
        }

        /// <summary>
        /// This function calls get API
        /// </summary>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        private static string CallGetAPI(string endpoint)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endpoint);
            request.Method = "GET";
            request.Headers.Add("ApiKey", apikey);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            string content = string.Empty;
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
            return content;
        }

        /// <summary>
        /// This function calls post API endpoint with data
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string CallPostAPI(string endpoint, byte[] data)
        {
            //Calling POS API to authenticate user credentials
            HttpWebRequest webRequest = WebRequest.Create(endpoint) as HttpWebRequest;
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.Headers.Add("ApiKey", apikey);

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
                stream.Close();
            }

            HttpWebResponse httwebresponse = (HttpWebResponse)webRequest.GetResponse();

            string content = string.Empty;
            using (Stream stream = httwebresponse.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
            httwebresponse.Dispose();
            return content;
        }

        public static string GetQuickBooksAccount()
        {
            //Calling GetbyClub POS API to get settings of POS app
            string endPoint = baseurl + Quickbooksaccountget + "?ClubId=" + clubid;

            return CallGetAPI(endPoint);
        }

        public static void LoadSettingsFromServer()
        {
            try
            {
                string content = GetQuickBooksAccount();
                var rootObject = JsonConvert.DeserializeObject<QuickBooksAccountRootObject>(content);
                if (rootObject != null)
                {
                    if (rootObject.Status == 1)
                    {
                        if (rootObject.Record != null)
                        {
                            if (rootObject.Record.QuickBooksAccount != null)
                            {
                                //Saving all the quickbooks related settings to Global class
                                DefaultQBAssetAccount = rootObject.Record.QuickBooksAccount.DefaultQBAssetAccount;
                                DefaultQBCOGSAccount = rootObject.Record.QuickBooksAccount.DefaultQBCOGSAccount;
                                DefaultQBIncomeAccount = rootObject.Record.QuickBooksAccount.DefaultQBIncomeAccount;
                                DefaultSalesTaxId = rootObject.Record.QuickBooksAccount.SalesTaxId;
                                DefaultSalesTaxAccount = rootObject.Record.QuickBooksAccount.SalesTaxAccount;
                                IsBarCodeEnabled = rootObject.Record.QuickBooksAccount.QBBarcodesEnabled;
                                TeeBookingQBId = rootObject.Record.QuickBooksAccount.TeeBookingQBId;
                                TeeGroupBookingQBId = rootObject.Record.QuickBooksAccount.TeeGroupBookingQBId;
                                MembershipPaymentQBId = rootObject.Record.QuickBooksAccount.MembershipPaymentQBId;
                                AuthorizeNetPaymentQBId = rootObject.Record.QuickBooksAccount.AuthorizeNetPaymentQBId;
                                BasysPaymentQBId = rootObject.Record.QuickBooksAccount.BasysPaymentQBId;
                                BluePayPaymentQBId = rootObject.Record.QuickBooksAccount.BluePayPaymentQBId;
                                BraintreePaymentQBId = rootObject.Record.QuickBooksAccount.BraintreePaymentQBId;
                                CashPaymentQBId = rootObject.Record.QuickBooksAccount.CashPaymentQBId;
                                CheckPaymentQBId = rootObject.Record.QuickBooksAccount.CheckPaymentQBId;
                                CreditVoucherPaymentQBId = rootObject.Record.QuickBooksAccount.CreditVoucherPaymentQBId;
                                FirstPayPaymentQBId = rootObject.Record.QuickBooksAccount.FirstPayPaymentQBId;
                                AnonymousCustomerQBId = rootObject.Record.QuickBooksAccount.AnonymousCustomerQBId;
                                TipQBId = rootObject.Record.QuickBooksAccount.TipQBId;
                                MemberShipItemQBId = rootObject.Record.QuickBooksAccount.MemberShipItemQBId;
                                DiscountItemQBId = rootObject.Record.QuickBooksAccount.DiscountItemQBId;
                                GiftVoucherItemQBId = rootObject.Record.QuickBooksAccount.GiftVoucherItemQBId;
                                RefundVoucherItemQBId = rootObject.Record.QuickBooksAccount.RefundVoucherItemQBId;
                                CreditVoucherSalesIncomeAccount = rootObject.Record.QuickBooksAccount.CreditVoucherSalesIncomeAccount;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\Log.txt", Environment.NewLine + DateTime.Now + " " + ex.Message + " " + ex.InnerException + " LoadSettingsFromServer clubid: " + GlobalClass.clubid);
            }
        }
    }
}
