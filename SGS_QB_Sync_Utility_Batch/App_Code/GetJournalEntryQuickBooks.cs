﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SGS_QB_Sync_Utility_Batch.App_Code
{
    class GetJournalEntryQuickBooks
    {
        //Declare an instance for log4net
        private static readonly ILog logger = LogManager.GetLogger(typeof(GetJournalEntryQuickBooks));

        public GetJournalEntryQuickBooks()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public void DoJournalEntryQuery()
        {
            try
            {
                XmlDocument requestXmlDoc = GlobalClass.GetXMLDocument();

                BuildJournalEntryQueryRq(requestXmlDoc, GlobalClass.GetInnerXMLforQB(requestXmlDoc));

                //Send the request and get the response from QuickBooks
                string responseStr = GlobalClass.SendRequestToQuickBooksDesktop(requestXmlDoc);

                WalkJournalEntryQueryRs(responseStr);

            }
            catch (Exception e)
            {
                logger.Error(e.Message + " " + e.StackTrace);
            }
        }


        void BuildJournalEntryQueryRq(XmlDocument doc, XmlElement parent)
        {

            //Create JournalEntryQueryRq aggregate and fill in field values for it
            XmlElement JournalEntryQueryRq = doc.CreateElement("JournalEntryQueryRq");
            parent.AppendChild(JournalEntryQueryRq);
            //Begin OR
            string ORElementType = "TxnID";
            if (ORElementType == "TxnID")
            {
                //Set field value for TxnID <!-- optional, may repeat -->
                JournalEntryQueryRq.AppendChild(GlobalClass.MakeSimpleElem(doc, "TxnID", "200000-1011023419"));
            }
            if (ORElementType == "RefNumber")
            {
                //Set field value for RefNumber <!-- optional, may repeat -->
                JournalEntryQueryRq.AppendChild(GlobalClass.MakeSimpleElem(doc, "RefNumber", "ab"));
            }
            

        }




        void WalkJournalEntryQueryRs(string response)
        {
            //Parse the response XML string into an XmlDocument
            XmlDocument responseXmlDoc = new XmlDocument();
            responseXmlDoc.LoadXml(response);

            //Get the response for our request
            XmlNodeList JournalEntryQueryRsList = responseXmlDoc.GetElementsByTagName("JournalEntryQueryRs");
            if (JournalEntryQueryRsList.Count == 1)
            {
                XmlNode responseNode = JournalEntryQueryRsList.Item(0);
                //Check the status code, info, and severity
                XmlAttributeCollection rsAttributes = responseNode.Attributes;
                string statusCode = rsAttributes.GetNamedItem("statusCode").Value;
                string statusSeverity = rsAttributes.GetNamedItem("statusSeverity").Value;
                string statusMessage = rsAttributes.GetNamedItem("statusMessage").Value;

                //status code = 0 all OK, > 0 is warning
                if (Convert.ToInt32(statusCode) == 0)
                {
                    XmlNodeList JournalEntryRetList = responseNode.SelectNodes("//JournalEntryRet");//XPath Query
                    for (int i = 0; i < JournalEntryRetList.Count; i++)
                    {
                        XmlNode JournalEntryRet = JournalEntryRetList.Item(i);
                        //WalkJournalEntryRet(JournalEntryRet);
                    }
                }
            }
        }
    }
}
